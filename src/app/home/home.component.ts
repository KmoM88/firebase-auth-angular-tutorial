import { FirebaseService } from './../services/firebase.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @Output() isLogout = new EventEmitter

  constructor(public firebaseService: FirebaseService) { }

  ngOnInit(): void {
  }

  signOut(){
    this.firebaseService.logout();
    this.isLogout.emit()
  }

}
