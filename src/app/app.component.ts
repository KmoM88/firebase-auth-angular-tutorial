import { FirebaseService } from './services/firebase.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isSignedIn: boolean = false;
  constructor(public firebaseService: FirebaseService){}
  ngOnInit(){
    if(localStorage.getItem('user')!==null)
      this.isSignedIn = true;
    else
      this.isSignedIn = false;
  }

  async onSignUp(email: string, psw:string){
    await this.firebaseService.signUp(email, psw)
    if(this.firebaseService.isLoggedIn)
      this.isSignedIn = true
  }

  async onSignIn(email: string, psw:string){
    await this.firebaseService.signIn(email, psw)
    if(this.firebaseService.isLoggedIn)
      this.isSignedIn = true
  }

  handleLougout(){
    this.isSignedIn = false;
  }
}
