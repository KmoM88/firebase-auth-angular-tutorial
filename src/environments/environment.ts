// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBjyAipgZVPLbH2fjyPhfasUqskg5OlXIc",
    authDomain: "ionic-firebase-chat-8d528.firebaseapp.com",
    projectId: "ionic-firebase-chat-8d528",
    storageBucket: "ionic-firebase-chat-8d528.appspot.com",
    messagingSenderId: "910309592088",
    appId: "1:910309592088:web:2a1c5c0dac02e33c02476d",
    measurementId: "G-F289HP8WDE"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
