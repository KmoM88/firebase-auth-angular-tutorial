import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth'

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  isLoggedIn: boolean = false;
  constructor(public firebaseAuth: AngularFireAuth) { }
  async signIn(email:string, psw:string) {
    await this.firebaseAuth.signInWithEmailAndPassword(email, psw)
            .then(res => {
              this.isLoggedIn = true;
              localStorage.setItem('user', JSON.stringify(res.user))
            })
  }

  async signUp(email:string, psw:string) {
    await this.firebaseAuth.createUserWithEmailAndPassword(email, psw)
            .then(res => {
              this.isLoggedIn = true;
              localStorage.setItem('user', JSON.stringify(res.user))
            })
  }

  logout() {
    this.firebaseAuth.signOut();
    localStorage.removeItem('user');
  }
}
